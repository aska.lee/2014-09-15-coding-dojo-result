REM usage RunAndCommit.bat"$(TargetPath)"
@echo off

git config user.name coder
git config user.email "coder@ntnu.com"

REM run unit tests
%1
if %errorlevel%==0 goto GIT_COMMIT
goto END

:GIT_COMMIT
pushd ..

echo ##### Got a green, ready to commit... #####
git add -A .
git commit -m "auto commit at green"
echo ##### End of commit #####
popd
exit 0


:END
