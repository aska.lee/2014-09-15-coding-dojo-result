#include "gmock/gmock.h"
#include <string>
#include "Code.h"
using namespace std;
using namespace ::testing;

//class MockTranslator: public ITranslator
//{
//public:
//	virtual string Translate(const string& source)
//	{
//		if(source == "Fizz") return "三人成虎";
//		if(source == "Buzz") return "五福臨門";
//		if(source == "Whizz") return "七星報喜";
//		return "translation error";
//	}
//};

class MockTranslator : public ITranslator {
public:
	MOCK_METHOD1(Translate, string(const string& source));
};

class MyTest : public ::testing::Test
{
};

// contain "3" ==> Fizz
// contain "5" ==> Buzz
// % 7 , contain 7 ==> Whizz
TEST_F(MyTest, Test1)
{
	FizzBuzzer fb;
	EXPECT_EQ("Fizz", fb.Convert(3));
	EXPECT_EQ("1", fb.Convert(1));
	EXPECT_EQ("2", fb.Convert(2));
	EXPECT_EQ("Buzz", fb.Convert(5));
	EXPECT_EQ("FizzBuzz", fb.Convert(15));
	EXPECT_EQ("FizzBuzz", fb.Convert(30));
	EXPECT_EQ("22", fb.Convert(22));

	EXPECT_EQ("Error", fb.Convert(0));
	EXPECT_EQ("Error", fb.Convert(-2));
	EXPECT_EQ("Error", fb.Convert(101));

	EXPECT_EQ("Fizz", fb.Convert(32));
	EXPECT_EQ("Buzz", fb.Convert(52));

	// % 3 != 0,  contain 3, 
	EXPECT_EQ("FizzBuzzWhizz", fb.Convert(35));
	// % 5 != 0,  contain 5, 
	EXPECT_EQ("FizzBuzz", fb.Convert(51));

	EXPECT_EQ("Whizz", fb.Convert(7));
	EXPECT_EQ("FizzWhizz", fb.Convert(21));



}



TEST_F(MyTest, UseTranslator)
{
	// Arrange
	MockTranslator mockTranslator;
	EXPECT_CALL(mockTranslator, Translate("Fizz"))              // #3
		.WillRepeatedly(Return("三人成虎"));
	EXPECT_CALL(mockTranslator, Translate("Buzz"))              // #3
		.WillRepeatedly(Return("五福臨門"));
	EXPECT_CALL(mockTranslator, Translate("Whizz"))              // #3
		.WillRepeatedly(Return("七星報喜"));

	FizzBuzzer fb(&mockTranslator);

	// Act
	string result = fb.Convert(3);

	// Assert
	EXPECT_EQ("三人成虎", result);

	EXPECT_EQ("五福臨門", fb.Convert(5));

	EXPECT_EQ("三人成虎五福臨門", fb.Convert(15));
	EXPECT_EQ("七星報喜", fb.Convert(7));
	EXPECT_EQ("1", fb.Convert(1));
}
