#pragma once
#include <string>

using namespace std;

class ITranslator
{
public:
	virtual string Translate(const string& source) = 0; // 要有virtual以及 = 0;
	virtual ~ITranslator() { } // 一定要有virtual destructor
};


class FizzBuzzer{
	ITranslator* m_pTranslator;
public:
	FizzBuzzer(ITranslator* pTranslator = NULL)
	{
		m_pTranslator = pTranslator;
	}
	std::string Convert(int n);
};