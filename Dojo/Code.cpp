#include "Code.h"
#include <sstream>

using namespace std;

bool ContainDigit(int number, int digit)
{
	do
	{
		if(number % 10 == digit)
			return true;
		else
			number /= 10;
	} while(number != 0);
	return false;
}


std::string FizzBuzzer::Convert(int n)
{	
	if(n <= 0 || n > 100) return "Error";

	std::string output;
	if(n % 3 == 0 || ContainDigit(n, 3))
	{
		if(m_pTranslator)
			output += m_pTranslator->Translate("Fizz");
		else
			output += "Fizz";
	}
	if(n % 5 == 0 || ContainDigit(n, 5))
	{
		if(m_pTranslator)
			output += m_pTranslator->Translate("Buzz");
		else
			output += "Buzz";

	}
	if(n % 7 == 0 || ContainDigit(n, 7))
	{
		if(m_pTranslator)
			output += m_pTranslator->Translate("Whizz");
		else
			output += "Whizz";
	}
	
	if(output.empty())
	{

		ostringstream oss;
		oss << n;
		return oss.str();
	}
	else
	{
		return output;
	}
}

