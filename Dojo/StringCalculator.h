#pragma once
#include <string>
#include <ostream>

struct HeaderAndBody
{
	std::string m_header;
	std::string m_body;
	HeaderAndBody(const std::string& header, const std::string& body)
		: m_header(header)
		, m_body(body)
	{

	}

	bool operator==(const HeaderAndBody& other) const
	{
		return m_header == other.m_header && m_body == other.m_body;
	}
};

std::ostream& operator<<(std::ostream& o, const HeaderAndBody& data);

int Add(std::string numbers);